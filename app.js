var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server)

var users = {};
var port = process.env.PORT || 2000;

app.get('/', function(request, resolve) {
    resolve.sendFile(__dirname + '/index.html');
})

server.listen(port);
console.log("listening at " + port);

//socket new client connection 
io.sockets.on('connection', function(socket) {

    // add key value pair for ip with no_of_browser_tabs_open    
    var ip = (socket.handshake.headers['x-forwarded-for'] ||
        socket.handshake.address);
    if (!users[ip]) {
        users[ip] = 1
    } else {
        users[ip] += 1;
    }
    console.log(users)
    setInterval(function() {
        socket.emit('users', {
            users: users
        });
    }, 1000);

    //Disconnect	
    socket.on('disconnect', function() {
        //remove one browser_tab_count from client ip 
        users[ip] -= 1;
        //no tabs are exist then remove client entry
        if (users[ip] == 0) {
            delete users[ip];
        }
        console.log(users)
    })


})